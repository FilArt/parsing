import requests
import cv2
import pytesseract
import uuid
from lxml import html
import os

FOLDER = 'img/'
DOMAIN = 'https://elcontacto.ru'


class Pipeline(object):

    def open_spider(self, spider):
        self.file = open('result.csv', 'a')

    def close_spider(self, spider):
        self.file.close()

    def process_item(self, item, spider):

        url = DOMAIN + item['href']
        response = requests.get(url)
        tree = html.fromstring(response.text)

        name = tree.xpath('//dd[@id="seller"]/strong')[0].text.strip()
        city = tree.xpath('//dd[@id="city"]')[-1].text.strip().capitalize()

        _id = tree.xpath('//span[@id="item_id"]')[0].text
        if not _id:
            return
        img_url = 'https://elcontacto.ru/?mod=item&op=make_phone_image&item_id={}'.format(_id)
        img_response = requests.get(img_url)

        img_response.raise_for_status()

        img_path = FOLDER + uuid.uuid4().hex
        with open(img_path, 'wb') as f:
            for chunk in img_response.iter_content(1024):
                f.write(chunk)

        img = cv2.imread(img_path)
        phone = ''.join(filter((lambda x: x != ' '), pytesseract.image_to_string(img)))

        os.remove(img_path)

        print(name, city, phone)
        self.file.write(','.join((name, city, phone)) + '\n')
