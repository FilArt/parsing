# -*- coding: utf-8 -*-
import scrapy


class PhonesSpider(scrapy.Spider):
    name = 'phones'
    allowed_domains = ['elcontacto.ru']
    start_urls = ['https://elcontacto.ru/ispaniya/']

    def parse(self, response):
        for ref in response.xpath('//td/h3/a'):
            yield {'href': ref.attrib['href']}

        for next_page in response.css('#NextLink'):
            yield response.follow(next_page, self.parse)
